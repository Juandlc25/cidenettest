import axios from "axios";
import React, { useEffect, useState } from "react";
import ErrorNot from "../../components/ErrorNot";
import Header from "../../components/Header";
import SuccessfullyMsg from "../../components/SuccessfullyMsg";
import { removeSpace, formValidation } from "../../utils/utils";
import "./style.css";

function Home() {
  const [lastName, setLastName] = useState("");
  const [surname, setSurname] = useState("");
  const [name, setName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [country, setCountry] = useState("");
  const [typeOfID, setTypeOfID] = useState("");
  const [idNumber, setIdNumber] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState();
  const [successfully, setSuccessfully] = useState();

  useEffect(() => {
    setLastName(removeSpace(lastName));
    setSurname(removeSpace(surname));
  }, [lastName, surname]);

  useEffect(() => {
    formValidation(lastName, name, idNumber, country, setEmail);
  }, [lastName, name, idNumber, country]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const newEmployee = {
        lastName,
        surname,
        name,
        middleName,
        country,
        typeOfID,
        idNumber,
        email,
      };
      await axios.post("http://localhost:9000/employees/register", newEmployee);
      setSuccessfully("Successfully added, Well done!");
      setEmail("");
      setLastName("");
      setMiddleName("");
      setName("");
      setSurname("");
      setTypeOfID("");
      setCountry("");
      setIdNumber("");
    } catch (err) {
      err.response.data.msg && setError(err.response.data.msg);
    }
  };
  return (
    <>
      <Header />
      <div className="home">
        <h1>Register Employee</h1>
        {error && <ErrorNot msg={error} clear={() => setError(undefined)} />}
        <form>
          <h5>Last name</h5>
          <input
            required
            style={{ "text-transform": "uppercase" }}
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            type="text"
          />
          <h5>Surname</h5>
          <input
            style={{ "text-transform": "uppercase" }}
            value={surname}
            onChange={(e) => setSurname(e.target.value)}
            type="text"
          />
          <h5>Name</h5>
          <input
            style={{ "text-transform": "uppercase" }}
            value={name}
            onChange={(e) => setName(e.target.value)}
            type="text"
          />
          <h5>Middle Name</h5>
          <input
            style={{ "text-transform": "uppercase" }}
            value={middleName}
            onChange={(e) => setMiddleName(e.target.value)}
            type="text"
          />
          <h5>Country</h5>
          <select value={country} onChange={(e) => setCountry(e.target.value)}>
            <option select value="">
              Select your country:
            </option>
            <option value="colombia">Colombia</option>
            <option value="usa">Estados Unidos</option>
          </select>
          <h5>Type of ID</h5>
          <select
            value={typeOfID}
            onChange={(e) => setTypeOfID(e.target.value)}
          >
            <option select value="">
              Select your type of ID:
            </option>
            <option value="cedula de cuidadania">Cédula de cuidadanía</option>
            <option value="cedula de extranjeria">Cédula de extranjería</option>
            <option value="pasaporte">Pasaporte</option>
            <option value="permiso especial">Permiso especial</option>
          </select>
          <h5>ID number</h5>
          <input
            value={idNumber}
            onChange={(e) => setIdNumber(e.target.value)}
            type="text"
          />
          <h5>Email</h5>
          <input
            style={{ "text-transform": "uppercase" }}
            value={email}
            disabled
            type="text"
          />
          {successfully && (
            <SuccessfullyMsg
              msg={successfully}
              clear={() => setSuccessfully(undefined)}
            />
          )}
          <button type="submit" className="home__btn" onClick={handleSubmit}>
            Submit
          </button>
        </form>
      </div>
    </>
  );
}

export default Home;
