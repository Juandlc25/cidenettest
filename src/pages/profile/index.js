import React, { useEffect, useState } from "react";
import Header from "../../components/Header";
import { useStateValue } from "../../contextAPI/StateProvider";
import axios from "axios";
import "./style.css";
import Employees from "../../components/Employees";

function Profile() {
  const [employees, setEmployees] = useState([]);
  const [{ user }] = useStateValue();
  useEffect(() => {
    const getEmployees = async () => {
      let employeesInfo = await axios.get(
        "http://localhost:9000/employees/find"
      );
      setEmployees(employeesInfo.data);
    };
    getEmployees();
  }, []);
  return (
    <>
      <Header profile />
      {employees.length === 0 ? (
        <div className="profile">
          <h2>Your employee list is empty</h2>
          <p>
            You have no employees in your list. To put some employees here, you
            have to add them in the main section of the application
          </p>
        </div>
      ) : (
        <div className="profile">
          <h2>Hello {user?.username}, this is your list of employees</h2>
          <Employees employees={employees} />
        </div>
      )}
    </>
  );
}

export default Profile;
