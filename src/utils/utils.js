export const removeSpace = (string) => {
  if (string.length === 0) {
    return "";
  } else {
    return string.split(" ").join("");
  }
};

export const formValidation = (lastName, name, idNumber, country, setEmail) => {
  if (lastName === "") {
    setEmail("");
  }
  if (lastName !== "") {
    setEmail(`${lastName}`);
  }
  if (name !== "") {
    setEmail(`${name}.${lastName}`);
  }
  if (idNumber !== "") {
    setEmail(`${name}.${lastName}.${idNumber}`);
  }
  if (country === "colombia") {
    setEmail(`${name}.${lastName}.${idNumber}@cidenet.com.co`);
  }
  if (country === "usa") {
    setEmail(`${name}.${lastName}.${idNumber}@cidenet.com.us`);
  }
};
