export const initialState = {
  token: undefined,
  user: undefined,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_USER":
      return {
        ...state,
        user: action.user,
        token: action.token,
      };
    default:
      return state;
  }
};

export default reducer;
