import React, { useEffect, useState } from "react";
import "./style.css";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import axios from "axios";
import { formValidation, removeSpace } from "../../utils/utils";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function Employee({ employees }) {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);
  const [id, setId] = useState("");
  const [lastName, setLastName] = useState("");
  const [surname, setSurname] = useState("");
  const [name, setName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [country, setCountry] = useState("");
  const [typeOfID, setTypeOfID] = useState("");
  const [idNumber, setIdNumber] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState();
  const [successfully, setSuccessfully] = useState();

  const handleSubmit = async (
    id,
    lastName,
    surname,
    name,
    middleName,
    country,
    typeOfID,
    idNumber,
    email
  ) => {
    setLastName(lastName);
    setSurname(surname);
    setName(name);
    setMiddleName(middleName);
    setCountry(country);
    setTypeOfID(typeOfID);
    setIdNumber(idNumber);
    setEmail(email);
    setId(id);
    setOpen(true);
  };
  useEffect(() => {
    setLastName(removeSpace(lastName));
    setSurname(removeSpace(surname));
  }, [lastName, surname]);

  useEffect(() => {
    formValidation(lastName, name, idNumber, country, setEmail);
  }, [lastName, name, idNumber, country]);
  const updateEmployee = async () => {
    try {
      await axios.put(`http://localhost:9000/employees/find/${id}`, {
        lastName,
        surname,
        name,
        middleName,
        country,
        typeOfID,
        idNumber,
        email,
      });
      setOpen(false);
    } catch (err) {
      console.log(err);
    }
  };
  const body = (
    <div style={modalStyle} className={`${classes.paper} modal`}>
      <h2>Update employee data</h2>
      <form>
        <h5>Last name</h5>
        <input
          required
          style={{ "text-transform": "uppercase" }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          type="text"
        />
        <h5>Surname</h5>
        <input
          style={{ "text-transform": "uppercase" }}
          value={surname}
          onChange={(e) => setSurname(e.target.value)}
          type="text"
        />
        <h5>Name</h5>
        <input
          style={{ "text-transform": "uppercase" }}
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="text"
        />
        <h5>Middle Name</h5>
        <input
          style={{ "text-transform": "uppercase" }}
          value={middleName}
          onChange={(e) => setMiddleName(e.target.value)}
          type="text"
        />
        <h5>Country</h5>
        <select value={country} onChange={(e) => setCountry(e.target.value)}>
          <option select value="">
            Select your country:
          </option>
          <option value="colombia">Colombia</option>
          <option value="usa">Estados Unidos</option>
        </select>
        <h5>Type of ID</h5>
        <select value={typeOfID} onChange={(e) => setTypeOfID(e.target.value)}>
          <option select value="">
            Select your type of ID:
          </option>
          <option value="cedula de cuidadania">Cédula de cuidadanía</option>
          <option value="cedula de extranjeria">Cédula de extranjería</option>
          <option value="pasaporte">Pasaporte</option>
          <option value="permiso especial">Permiso especial</option>
        </select>
        <h5>ID number</h5>
        <input
          value={idNumber}
          onChange={(e) => setIdNumber(e.target.value)}
          type="text"
        />
        <h5>Email</h5>
        <input
          style={{ "text-transform": "uppercase" }}
          value={email}
          disabled
          type="text"
        />
        <button onClick={updateEmployee}>Update employee</button>
      </form>
    </div>
  );
  return (
    <>
      {employees.slice(0, 10).map((employee) => (
        <tr>
          <td>{employee.lastName}</td>
          <td>{employee.surname}</td>
          <td>{employee.name}</td>
          <td>{employee.middleName}</td>
          <td>{employee.country}</td>
          <td>{employee.typeOfID}</td>
          <td>{employee.idNumber}</td>
          <td>{employee.email}</td>
          <td>{employee.status}</td>
          <td>{employee.created_at}</td>
          <td className="employee">
            <button
              className="employee__btn"
              onClick={() =>
                handleSubmit(
                  employee._id,
                  employee.lastName,
                  employee.surname,
                  employee.name,
                  employee.middleName,
                  employee.country,
                  employee.typeOfID,
                  employee.idNumber,
                  employee.email
                )
              }
            >
              Edit
            </button>
          </td>
        </tr>
      ))}
      <Modal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </>
  );
}

export default Employee;
