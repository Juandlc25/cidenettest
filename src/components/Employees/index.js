import React from "react";
import Employee from "../Employee";
import "./style.css";

function Employees({ employees }) {
  return (
    <div className="employees">
      <table>
        <tr>
          <th>Last Name</th>
          <th>Surname</th>
          <th>Name</th>
          <th>Middle Name</th>
          <th>Country</th>
          <th>Type Of ID</th>
          <th>ID Number</th>
          <th>Email</th>
          <th>Status</th>
          <th>Created At</th>
          <th>Edit</th>
        </tr>
        <Employee employees={employees} />
      </table>
    </div>
  );
}

export default Employees;
